# SKA MID simulations

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sim-mid/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-sim-mid/en/latest/?badge=latest)

See <https://confluence.skatelescope.org/display/SE/Simulations+with+Direction-Dependent+Effects>

The driver scripts are in first_pass, and the python code is in src.

## Telescope

* All dishes up to 10km from array centre --rmax 1e4
* Elevation-dependent beam model from dish deformation

## Sky model

* S3 extragalactic catalogue.
* Fields: at declinations of 15, 0, -30, -60, and -90 degrees --dec

## Atmospheric models

Ionospheric and tropospheric model ARatmosphpy (see from previous simulations) --screen

## Frequency setup

### Band 1 Low

* Frequency channels: 350 - 450 MHz in 10 MHz steps (10 channels)
* Effects to include: ionosphere

### Band 2

* Frequency channels: 1.3 - 1.4 GHz in no finer than 1 MHz steps (100 channels)
* Effects to include: ionosphere, troposphere, dish deformations, heterogeneous array

### Band 5

* Frequency channel: 12.5 GHz.
* Effects to include: troposphere, dish deformations

## Time sampling

### First pass

* Short observation: 1 hour with with 10s integrations and 10 10MHz channels.
* Long observation: up to 8 hours (as appropriate for declination) with 60s integrations and single channel.

### Second pass

* Medium observation: 1 hour with with 10s integrations and 10 10MHz channels.
* Long observation: up to 8 hours (as appropriate for declination) with 60s integrations and 10 10MHz channels.

### General considerations

The typical sizes of fully sampled, full polarisation continuum visibilities are:

* Dishes within 1km of centre: 0.92GB for 12 channels of 10.7MHz and 680s integration time, 49 wplanes
* Dishes within 10km of centre: 139GB for 107 channels of 1.07MHz and 80s integration time, 428 wplanes
* All MID dishes: 236TB for 1161 channels of 0.12MHz and 7.4s integration time, 4070 w planes

We have allowed 10% fractional bandwidth to fill in the uv sampling. The sampling is to provide no
more than 2% smearing of a point source at the half power point of the primary beam.

See first_pass/README.md for more details of the tests

The beam models are kept on GCP. To get a local copy:

```shell
gsutil -m rsync -r gs://ska1-simulation-data/ska1-mid/beam_models ./beam_models
```

## Contribute to this repository

We use [Black](https://github.com/psf/black) to keep the python code style in good shape. 
Please make sure you black-formatted your code before merging to master.

The linting step in the CI pipeline checks that the code complies with black formatting style,
and fails if that is not the case.
