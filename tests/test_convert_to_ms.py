import glob
import logging
import os
import shutil
import unittest

import matplotlib.pyplot as plt
import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration import (
    create_named_configuration,
)
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import export_visibility_to_hdf5, create_visibility

from src.convert_to_ms import cli_parser, do_merge, plot_vis

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.WARNING)


class TestConvertMS(unittest.TestCase):
    def setUp(self):

        self.persist = os.getenv("RASCIL_PERSIST", False)

        self.frequency = numpy.linspace(1e8, 1.5e8, 3)
        self.channelwidth = numpy.array([2.5e7, 2.5e7, 2.5e7])
        self.vis_pol = PolarisationFrame("stokesI")

        self.times = numpy.linspace(-300.0, 300.0, 3) * numpy.pi / 43200.0

        self.phasecentre = SkyCoord(
            ra=+180.0 * u.deg, dec=-60.0 * u.deg, frame="icrs", equinox="J2000"
        )

        self.config = create_named_configuration("MID", rmax=500.0)

        self.vis = create_visibility(
            self.config,
            self.times,
            self.frequency,
            channel_bandwidth=self.channelwidth,
            phasecentre=self.phasecentre,
            weight=1.0,
            polarisation_frame=self.vis_pol,
        )

        self.filename = "results/SKA_MID_SIM_test_0.hdf"
        parser = cli_parser()
        self.args = parser.parse_args(
            [
                "--hdffile",
                self.filename,
                "--plot",
                "True",
            ]
        )

    def test_do_merge(self):

        export_visibility_to_hdf5(self.vis, self.filename)
        out = do_merge(self.args)

        msname = self.filename.replace("_0.hdf", ".ms")
        visname = self.filename.replace("_0.hdf", "_vis.jpg")
        uvname = self.filename.replace("_0.hdf", "_uvcoverage.png")

        assert os.path.exists(msname)
        assert os.path.exists(visname)
        assert os.path.exists(uvname)

        if self.persist is False:

            os.remove(visname)
            os.remove(uvname)

    def test_plot_vis(self):

        msname = self.filename.replace("_0.hdf", ".ms")
        out = plot_vis(msname)

        assert plt.gcf().number == 3

        if self.persist is False:
            shutil.rmtree(msname)
            imglist = glob.glob("results/SKA_MID_SIM_test*")
            for f in imglist:
                os.remove(f)


if __name__ == "__main__":
    unittest.main()
