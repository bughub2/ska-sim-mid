import subprocess
from unittest.mock import Mock

import astropy.units as u
import numpy as np
import pytest
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility

from rfi.mid_rfi_simulation import add_noise, generate_ska_mid_configuration, _get_diam


def test_add_noise():
    """
    Random gaussian noise is added to visibility data.
    Input data is 0s everywhere. Output is the random noise.
    """
    # rmax = 30 m --> 1 baseline 30 m within SKA LOW centre.
    config = create_named_configuration("MID", rmax=30.0)
    times = np.array([-0.0001, 0.0001])  # hour angle in rad
    frequency = np.linspace(160e6, 200e6, 2)
    phasecentre = SkyCoord(
        ra=150.0 * u.deg, dec=-35.0 * u.deg, frame="icrs", equinox="J2000"
    )
    channel_bandwidth = np.array([1e7, 1e7])

    vis = create_visibility(
        config,
        times,
        frequency,
        phasecentre=phasecentre,
        weight=1.0,
        polarisation_frame=PolarisationFrame("linear"),
        channel_bandwidth=channel_bandwidth,
    )

    assert (vis["vis"].data == 0).all()

    result = add_noise(vis)
    assert (
        abs(result["vis"].data) > 0
    ).all()  # Noise is from a random normal distribution.


def test_generate_ska_mid_configuration_default_mid():
    """
    Returns default MID configuration via RASCIL with 197 dishes.
    """
    result = generate_ska_mid_configuration(Mock(), antenna_file=None)
    assert result.name == "MID"
    assert result.dims["id"] == 197


def test_generate_ska_mid_configuration_default_mid_rmax():
    """
    Returns default MID configuration via RASCIL
    with 5 dishes at 50 m from array centre.
    """
    result = generate_ska_mid_configuration(Mock(), antenna_file="", rmax=50)
    assert result.name == "MID"
    assert result.dims["id"] == 5


def test_generate_ska_mid_configuration_ant_file():
    """
    Returns the configuration generated from the given
    antenna file, which contains 197 stations.
    """
    git_root = subprocess.run(
        ["git", "rev-parse", "--show-toplevel"],
        capture_output=True,
        encoding="utf-8",
    ).stdout.strip("\n")

    result = generate_ska_mid_configuration(
        Mock(),  # log
        antenna_file=f"{git_root}/rfi/data/"
        f"aeronautical_v2_SKA_Mid_coord/SKA1MID.csv",
    )
    assert result.name == "MID_AERO"
    assert result.dims["id"] == 197


def test_generate_ska_mid_configuration_ant_file_rmax():
    """
    Returns the configuration generated from the given
    antenna file, which contains 197 stations.

    Even if rmax is set, the result is the same,
    since rmax is not used when antenna_file is specified
    """
    git_root = subprocess.run(
        ["git", "rev-parse", "--show-toplevel"],
        capture_output=True,
        encoding="utf-8",
    ).stdout.strip("\n")

    result = generate_ska_mid_configuration(
        Mock(),  # log
        antenna_file=f"{git_root}/rfi/data/"
        f"aeronautical_v2_SKA_Mid_coord/SKA1MID.csv",
        rmax=50,
    )
    assert result.name == "MID_AERO"
    assert result.dims["id"] == 197


def test_get_diam_meerkat():
    result = _get_diam("M01233")
    assert result == 13.5


def test_get_diam_ska():
    result = _get_diam("SKA4342b")
    assert result == 15.0


def test_get_diam_error():
    with pytest.raises(ValueError):
        _get_diam("bla")
