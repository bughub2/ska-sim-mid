import argparse
import logging

from rascil.processing_components.visibility import (
    create_visibility_from_ms,
    convert_visibility_stokesI_to_polframe,
    export_visibility_to_ms,
)
from rascil.workflows.rsexecute.execution_support import rsexecute, get_dask_client
from rascil.workflows.rsexecute.imaging import subtract_list_rsexecute_workflow
from rascil.workflows.rsexecute.visibility import (
    concatenate_visibility_time_rsexecute,
)
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame


def cli_parser():
    parser = argparse.ArgumentParser(
        description="Calculate difference between nominal and actual simulation results."
    )

    parser.add_argument(
        "--actual_ms",
        type=str,
        default="",
        help="Input MeasurementSet containing 'actual' (i.e. with errors)"
        " observation simulations",
    )

    parser.add_argument(
        "--nominal_ms",
        type=str,
        default="",
        help="Input MeasurementSet containing 'nominal' (i.e. without any errors)"
        " observation simulations",
    )

    parser.add_argument(
        "--output_dir",
        type=str,
        default="./",
        help="Directory for results",
    )

    parser.add_argument(
        "--use_dask",
        type=str,
        default="True",
        help="Use dask processing?",
    )

    return parser


def calculate_and_save_difference(args):
    if args.actual_ms == "":
        raise ValueError("Please provide MS with actual values")

    if args.nominal_ms == "":
        raise ValueError("Please provide MS with nominal values")

    logfile = f"{args.output_dir}/{args.actual_ms.split('/')[-1].replace('actual.ms', 'diff.log')}"

    def init_logging():
        logging.basicConfig(
            filename=logfile,
            filemode="a",
            format="%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
            datefmt="%H:%M:%S",
            level=logging.INFO,
        )

    init_logging()
    log = logging.getLogger("ska-sim-mid-logger")

    if args.use_dask == "True":
        client = get_dask_client()
        rsexecute.set_client(use_dask=True, client=client)
    else:
        rsexecute.set_client(use_dask=False)

    actual_bvis_list = create_visibility_from_ms(args.actual_ms)
    nominal_bvis_list = create_visibility_from_ms(args.nominal_ms)

    difference_bvis_list = subtract_list_rsexecute_workflow(
        actual_bvis_list, nominal_bvis_list
    )

    # Convert to required polarisation
    difference_bvis_list = [
        rsexecute.execute(convert_visibility_stokesI_to_polframe)(
            bvis, PolarisationFrame(actual_bvis_list[i]._polarisation_frame)
        )
        for i, bvis in enumerate(difference_bvis_list)
    ]
    difference_bvis_list = rsexecute.persist(difference_bvis_list)

    # Do the concatenate and write in the cluster
    out_msname = f"{args.output_dir}/{args.actual_ms.split('/')[-1].replace('actual.ms', 'difference.ms')}"

    ms_exec = [concatenate_visibility_time_rsexecute(difference_bvis_list)]
    ms_exec = rsexecute.execute(export_visibility_to_ms)(out_msname, ms_exec)
    rsexecute.compute(ms_exec, sync=True)

    log.info("Difference written to %s", out_msname)

    rsexecute.close()


def main():
    parser = cli_parser()
    args = parser.parse_args()
    calculate_and_save_difference(args)


if __name__ == "__main__":
    main()
