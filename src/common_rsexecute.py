"""Dask functions to support simulations from components

"""

__all__ = ["predict_gaintable_components_list_rsexecute_workflow"]

import logging

from rascil.processing_components.calibration import apply_gaintable
from rascil.processing_components.imaging import dft_skycomponent_visibility
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute

log = logging.getLogger("ska-sim-mid-logger")


def predict_gaintable_components_list_rsexecute_workflow(
    obsvis, gt_list, components_list, docal=False, inverse=True, **kwargs
):
    """Predict from a list of skycomponents

    If obsvis is a list then we pair obsvis element and skymodel_list element and predict
    If obvis is Visibility then we calculate Visibility for each skymodel

    :param obsvis: Observed Visibility
    :param gt_list: List of gaintables, one per component
    :param components_list: List of components
    :param docal: Apply calibration table in skymodel
    :param kwargs: Parameters for functions in components
    :return: List of vis_lists
    """

    def ft_cal_sc(sc_list, gtl, ov):
        """Predict visibility for a skymodel

        :param gt: Gaintable
        :param sc: SkyComponents
        :param ov: Input visibility
        :return: Visibility with dft of components, gaintable applied
        """
        output_vis = ov.copy(deep=True, zero=True)
        sc_vis = ov.copy(deep=True, zero=True)
        for isc, sc in enumerate(sc_list):
            sc_vis = dft_skycomponent_visibility(sc_vis, sc, **kwargs)
            if docal and gtl[isc] is not None:
                sc_vis = apply_gaintable(sc_vis, gtl[isc], inverse=inverse)
            output_vis["vis"] += sc_vis["vis"]
        return output_vis

    return rsexecute.execute(ft_cal_sc)(components_list, gt_list, obsvis)
