#!/bin/sh
# This scripts is specific to Maciej's test
# Please contact Maciej Serylak for details

SSMRESULTS=${HOME}/data/ska_sim_mid/maciej/
SSMROOT=${HOME}/Code/ska-sim-mid
vp_directory=${SSMROOT}/resources/beam_models/
PYTHONPATH=$PYTHONPATH:${SSMROOT}

python3 ${SSMROOT}/src/mid_simulation.py --mode wind_pointing \
  --wind_conditions standard --flux_limit 0.7 --declination -45 --band B2 --pbtype MID_B2 \
  --results ${SSMRESULTS} --rmax 1e4 \
  --memory 0 --duration medium --configuration MID \
  --vp_directory ${vp_directory} --nchan 8 --use_dask False
