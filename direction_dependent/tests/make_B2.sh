#!/usr/bin/env bash

#! Set essential environment variables
if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${HOME}/data/ska_mid_simulations/results
fi

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi

if [ -z "$SSMRESOURCES" ]
then 
    SSMRESOURCES=/alaska/shared/ska-sim-mid/ # only on P3
fi

echo "SSMROOT : $SSMROOT"
echo "SSMRESULTS : $SSMRESULTS"
echo "SSMRESOURCES: $SSMRESOURCES"

vp_directory=${SSMRESOURCES}/beam_models/
screens=${SSMRESOURCES}/screens


for mode in surface random_pointing wind_pointing polarisation
  do
    echo "Processing" ${mode}
    python3 ${SSMROOT}/src/mid_simulation.py --mode ${mode}  --flux_limit 0.01 \
      --declination -45.0 --band B2 --pbtype MID_B2 --results ${SSMRESULTS} --rmax 1e3 \
      --duration custom --integration_time 600.0 \
      --screen ${screens}/tropo_screen_long.fits --height 3000 \
      --configuration MID --vp_directory ${vp_directory} --nchan 4
  done

for mode in  heterogeneous troposphere
  do
    echo "Processing" ${mode}
    python3 ${simroot}/src/mid_simulation.py --mode ${mode}  --flux_limit 0.01 \
      --declination -45.0 --band B2 --pbtype MID_B2 --results ${results_dir} --rmax 1e3 \
      --duration custom --integration_time 600.0 \
      --screen ${screens}/tropo_screen_long.fits --height 3000 \
      --configuration MID --vp_directory ${vp_directory} --nchan 4
  done

