#!/usr/bin/env bash

#! Set essential environment variables
if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${HOME}/data/ska_mid_simulations/results
fi

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi

if [ -z "$SSMRESOURCES" ]
then
    SSMRESOURCES=/alaska/shared/ska-sim-mid/ # only on P3
fi

echo "SSMROOT : $SSMROOT"
echo "SSMRESULTS : $SSMRESULTS"
echo "SSMRESOURCES: $SSMRESOURCES"

vp_directory=${SSMRESOURCES}/beam_models/
screens=${SSMRESOURCES}/screens

for nchan in 8
  do
    for duration in long
      do
        for dec in -45
          do
            python3 ${SSMROOT}/src/mid_simulation.py --mode wind_pointing  --flux_limit 1e-05 \
              --declination ${dec} --band Ku --pbtype MID_Ku --results ${SSMRESULTS} --rmax 1e4 \
              --duration ${duration}    \
              --screen ${screens}/tropo_screen_long.fits --height 3000 \
              --configuration MID --vp_directory ${vp_directory} --nchan ${nchan}
          done
      done
  done

