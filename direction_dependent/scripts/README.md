
In this directory we have example driver scripts for use of the RASCIL-based simulation script
mid_simulation.py. The scripts will need to be altered for ocation of the various files needed.

The main purpose of the mid_simulation.py script is to simulate visibility data observed by SKA 
observations in the presence of any of a number of physical effects afflicting the antenna 
gain. The effects are selected by the --mode command line argument, and are listed below.

The help for mid_simulation.py can be viewed by:

    python mid_simulation.py --help

All the  driver scripts (e.g. wind_pointing/make_B2.sh) can loop over duration and declination.

These simulations work best on the P3 cluster or login node. See https://confluence.skatelescope.
org/display/SE/P3+How+To

If you are running on your own machine, make sure you have the below environment variables set up:

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder

The default or these variables are set as common directories on P3.

There are two outputs: Images and MeasurementSets. If the argument --mode is nominal, those outputs 
will correspond to the data observed without any gain errors. If the mode is not nominal and 
is one of those listed below then the results for corrupted (i.e. with gain errors) and differences 
(with gain errors minus without gain errors) are written.

In addition to the --mode command line arguments, there are three particularly important other
command line arguments:

--rmax: The maximum radius of antennas used in the simulation from the array centre.
If rmax is set too large then calculating the dirty images can take a long time. 

--npixel: The size of the image on each axis e.g. 1024 gives a 1024 x 1024 imageIf this is set large
the imaging will take a longer

--nchan: The number of channels in the simulation. Only nchan greater than 1
unless you have a good reason. The processing time will scale with nchan.

The images can be viewed using the casaviewer or carta. The MeasurementSets can be viewed
using casaviewer. 

The modes supported are:

### nominal
    All bands
    Performance without any gain errors added
    All stokes

### heterogenous
    B2 only
    Simulates observations with all SKA and MEERKAT dishes (on) and all SKA dishes (off).
    All stokes

### heterogenous_meerkat+
    B2 only
    Simulates observations with MEERKAT+ dishes (on) and all SKA dishes (off).
    All stokes

### ionosphere
    This requires the screens to be calculated first: see ../screens/README.md
    B1LOW
    Simulates observations with ionospheric screens on and off.
    Stokes I only

### polarisation
    B2
    Simulates observations with SKA with cross pol (on) and SKA no cross pol (off)
    Stokes stokesIQUV, linear

### surface
    B2 only, B5 beams are inconsistent as is
    Simulates observations with sagging dish (on) and nominal dish (off)
    Stokes I only

### troposphere
    This requires the screens to be calculated first: see ../screens/README.md
    B2 and B5
    Simulates observations with tropospheric screens on and off.
    Stokes I only

### wind_pointing
    B2 and B5
    Simulates observations with wind buffeting of dishes (on) and without (off)
    Stokes I only
