.. _direction_dependent-guide:

***************************
Direction-dependent effects
***************************

These scripts simulate MID observations of multiple point sources and calculate the effect of direction dependent gain
errors.

    - The sky model is constructed from Oxford S3-SEX catalog. These are unpolarised point sources.
    - The observation is by MID or MEERKAT+ over a range of hour angles.
    - The visibility is calculated by Direct Fourier transform  after application of the gaintable for each source.
    - Dask is used to distribute the processing over a number of workers.
    - Processing can be divided into chunks of time (default 1800s). This allows

See the following presentation for an overview of the results:

    `Simulating MID direction dependent gain effects SPO-1057 <https://docs.google.com/presentation/d/1NfhdqEHHfXl_MvZnfrCh0Je1JQrJjRdGb39tyUU2V7c>`_

The core simulation functions reside in RASCIL. The RASCIL driver script in this repository is
direction_dependent/src/mid_simulation.py. This can be run directly using the command line
arguments listed below, or some typical bash scripts have been provided in direction_dependent_src/scripts.
The bash scripts allow for looping over duration and declination. The scripts will need to be altered for
location of the various files needed. We recommend use of the bash scripts at first.

Note that the simulation has two steps: first the visibilities are calculated and written to HDF files, using
mid_simulation.py and then all the HDF files are combined into one MeasurementSet using convert_to_ms.py. In this
conversion step, a number of diagnostic plots are written.

These simulations typically requiring a cluster to run. In Cambridge They work well on the P3 login node, which has
512GB and 64 threads/32 cores, using a large number of threads. For this approach --use_slurm should be False.

There are types of output: MeasurementSets and Images. Each are calculated
for actual, nominal, and difference. The time consumed in the calculation of
the MeasurementSets is small compared to the time in writing the MeasurementSet
and the time to make the images. Images may be calculated using the dist_imager.

The fits images can be viewed using the casaviewer or carta. The MeasurementSets can be viewed
using casaviewer.

If you are running on your own machine, make sure you have the below environment variables set up:

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder

The default or these variables are set as common directories on P3.

Effects simulated
=================

Nominal
#######

- A set of point sources is simulated and the relevant nominal voltage pattern for each end of interferometer is
  applied before Fourier transform. The nominal pattern is constructed from a tapered symmetric illumination pattern,
  with the diameter of the SKA dishes.
- stokesIQUV, band B2


Heterogenous
############

- A set of point sources is simulated and the relevant voltage pattern for each end of interferometer is
  applied before Fourier transform.
- Simulates observations with all SKA and MEERKAT dishes (on state) and all SKA dishes (off state).
- stokesIQUV, band B2

Heterogenous_meerkat+
#####################

- A set of point sources is simulated and the relevant voltage pattern for each end of interferometer is applied
  before Fourier transform.
- Simulates observations with MEERKAT+ configuration, all SKA and MEERKAT dishes (on state) and all SKA dishes (off
  state).
- stokesIQUV, band B2


Ionosphere
##########

- A set of point sources is simulated and the phases calculated using a thin screen model for the
  ionosphere. The screen has units of meters of Total Electron Content. The phase is evaluated at
  places in the screen where the line of sight from source to a dish pierces the screen.
- Simulates observations with ionospheric screens on and off.
- This requires the screens to be calculated first or downloaded: see ../screens/README.md
- stokesI, band B1LOW


Polarisation
############

- Simulates observations with SKA and EMSS calculated primary beam models with cross pol (on) and no cross pol set to
  zero (off)
- Polarisation stokesIQUV, linear, band B2

Surface sag
###########

- Simulates observations with sagging dish (on) and nominal dish (off).
- Models of the voltage pattern are available at +15, +45, +90 deg elevation.
- We interpolate between those to 5 degrees.
- Stokes I, band B2

For more details see: https://confluence.skatelescope.org/display/SE/Dish+deformation+simulations

Troposphere
###########

- Simulates observations with tropospheric screens on and off.
- This requires the screens to be calculated first or downloaded: see ../screens/README.md
- stokesI, bands B2 and B5
- A set of point sources is simulated and the phases calculated using a thin screen model for the
  atmosphere. The phase is evaluated at places in the screen where the line of sight from source to
  dish pierces the screen. The screen has units of meters of delay.

Wind pointing
#############

- Simulates observations with wind buffeting of dishes (on) and without (off)
- Stokes I, bands B2 and B5

For more details see: https://confluence.skatelescope.org/display/SE/MID+pointing+error+simulations

.. argparse::
   :module: mid_simulation
   :func: cli_parser
   :prog: mid_simulation.py
