.. _PSI-guide:

***************
PSI simulations
***************

These are simulated visibility data sets in Measurement Set format, as needed to test the operation of the CBF-SDP
interface and the real-time signal displays.

These simulations generate fully time-sampled, high number of spectral channel observations using the simulated Prototype System
Integration (PSI) test arrays:  a small number of antennas or stations (between 4 and 8), with a large number of
frequency channels (of order 10000).

The simulation currently does:

 - 10 minutes of 0.28s integrations
 - six antennas chosen to be close to the centre but to give baselines upto 200m
 - 50% fractional bandwidth: centre frequency 1.369GHz +/- 0.5 * 1.369GHz

The simulation requires a large amount of memory, primality because of inefficiencies in the RASCIL
visibility format. We expect to reduce these over time. The best Dask setup on Alaska P3 is to
use one worker and a large number of threads (up to 64).

.. argparse::
   :module: mid_write_psi
   :func: cli_parser
   :prog: mid_write_psi.py

.. todo::
    - Insert todo's here
