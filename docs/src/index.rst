.. _documentation_master:

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

ska-sim-mid
===========

This package collects RASCIL scripts for various SKA-MID simulations: continuum imaging, continuum
imaging with direction-dependent effects, high data rate observations, observation sizing,
and radio frequency interference.

The package uses `RASCIL <https://gitlab.com/ska-telescope/external/rascil-main.git>`_ for workflow functions,
and `ska-sdp-datamodels <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels.git>`_ for data models.

.. toctree::
  :maxdepth: 1

  continuum_imaging/guide
  direction_dependent/guide
  PSI/guide
  sizing/guide
  rfi

Instructions
============

To install these scripts, use git::

    git clone https://gitlab.com/ska-telescope/sim/ska-sim-mid.git


Or download the source code from https://gitlab.com/ska-telescope/sim/ska-sim-mid

None of these simulations need to be installed into the python path. Instead please set the below environment variables::

    export SSMROOT=/path/to/code
    export SSMRESULTS=/path/to/results

In direction dependent simulations, one will also need the additional data resources (e.g. beam models) which requires setting the below environment variable::

    export SSMRESOURCES=/path/to/resources

RASCIL must be installed - the options are via pip, git clone, or docker. The simplest approach is to use pip. In the
ska-sim-mid directory, do::

    pip3 install -r requirements.txt

A script can be run as::

    export SMSROOT=`pwd`
    cd sizing/scripts
    sh make_sizes.sh

The resource files for the SKA direction-dependent MID simulations are 80GB in size and are kept on the Google Cloud
Platform. The python command line tool gsutil allows for interacting with the Google Cloud Platform::

    https://cloud.google.com/storage/docs/gsutil


After installing gsutil, you may download the resources as follows::

    cd resources
    gsutil -m rsync -r gs://ska1-simulation-data/resources/mid/beam_models/ beam_models
    gsutil -m rsync -r gs://ska1-simulation-data/resources/shared/screens screens

