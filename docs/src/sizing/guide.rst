.. _sizing-guide:

*******************
Sizing observations
*******************

This script estimates the size of CASA MeasurementSets for various MID observations. it does this by
constructing a scaled down Visibility and writing it to a MeasurementSet.

The maximum size of the array (maximum distance from the array centre) can be set. This determines
the angular resolution. See parameter rmax.

The field of view to be imaged is set to a multiple of the primary beam. See parameter guardband.

The time and frequency sampling is adjusted to match the SKA specification of 2% decorrelation at the
half-power point of the primary beam. See parameter dela.

.. argparse::
   :module: mid_size
   :func: cli_parser
   :prog: mid_size.py

Typical output::

    mid_size: Starting MID simulation sizing

    {'band': 'B2',
     'context': 's3sky',
     'declination': -45.0,
     'dela': 0.02,
     'fractional_bandwidth': 0.1,
     'guardband': 2.0,
     'integration_time': 80.0,
     'output_msname': 'mid_simulation.ms',
     'ra': 15.0,
     'rmax': 1000.0,
     'time_range': [-4.0, 4.0],
     'verbose': 'False'}
    mid_size: Using only dishes within 1000.0m of the array centre requires 12 channels of 1.13e+07Hz and integration time 683.8s, the MS size is 0.338GB
    mid_size: The time and frequency sampling is to provide no more than 2% smearing at the half power point of the primary beam.
    mid_size: Image has shape 768 by 768, 4 polarisations, and size 0.018GB
    mid_size: W processing requires 49 w planes of step 238.8 wavelengths and maximum support 0 pixels
    mid_size: Allowing fractional bandwidth 0.100  to fill in the uv sampling.
    nmid_size: Starting MID simulation sizing

    {'band': 'B2',
     'context': 's3sky',
     'declination': -45.0,
     'dela': 0.02,
     'fractional_bandwidth': 0.1,
     'guardband': 2.0,
     'integration_time': 80.0,
     'output_msname': 'mid_simulation.ms',
     'ra': 15.0,
     'rmax': 10000.0,
     'time_range': [-4.0, 4.0],
     'verbose': 'False'}
    mid_size: Using only dishes within 10000.0m of the array centre requires 107 channels of 1.27e+06Hz and integration time 80.4s, the MS size is 50.382GB
    mid_size: The time and frequency sampling is to provide no more than 2% smearing at the half power point of the primary beam.
    mid_size: Image has shape 8192 by 8192, 4 polarisations, and size 2.000GB
    mid_size: W processing requires 428 w planes of step 238.8 wavelengths and maximum support 12 pixels
    mid_size: Allowing fractional bandwidth 0.100  to fill in the uv sampling.
    nmid_size: Starting MID simulation sizing

    {'band': 'B2',
     'context': 's3sky',
     'declination': -45.0,
     'dela': 0.02,
     'fractional_bandwidth': 0.1,
     'guardband': 2.0,
     'integration_time': 80.0,
     'output_msname': 'mid_simulation.ms',
     'ra': 15.0,
     'rmax': 200000.0,
     'time_range': [-4.0, 4.0],
     'verbose': 'False'}
    mid_size: Using only dishes within 200000.0m of the array centre requires 1161 channels of 1.17e+05Hz and integration time 7.4s, the MS size is 8512.629GB
    mid_size: The time and frequency sampling is to provide no more than 2% smearing at the half power point of the primary beam.
    mid_size: Image has shape 98304 by 98304, 4 polarisations, and size 288.000GB
    mid_size: W processing requires 4065 w planes of step 238.8 wavelengths and maximum support 132 pixels
    mid_size: Allowing fractional bandwidth 0.100  to fill in the uv sampling.

