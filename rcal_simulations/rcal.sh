#!/usr/bin/env bash
if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${HOME}/Code/ska-sim-mid/rcal_simulations/
fi
echo "SSMRESULTS : $SSMRESULTS"

if [ -z "$SSMRESOURCES" ]
then
    SSMRESOURCES=${HOME}/Code/ska-sim-mid/resources
fi
echo "SSMRESOURCES : $SSMRESOURCES"

source=point # "double" or "s3sky"
results_dir=${SSMRESULTS}/${source}
mkdir -p ${results_dir}
cd ${results_dir} || exit 0

python3 ${RASCIL}/rascil/apps/rascil_rcal.py --ingest_msname \
  ${results_dir}/rcal.ms \
  --do_plotting True \
  --phase_only False \
  --plot_dynamic False \
