

#The SKA RCAL-RFI MID simulations 

The repository contains scripts that include:

   make\_${band}.sh: Script to generate MID simulations with custom source model (point, double or s3sky)
   rcal.sh: Run the RASCIL RCAL pipeline on the generated MS dataset.
   sim\_rfi\_mid\_aa05.py: Apply fake RFI signal to the MS dataset. Note this requires the HDF data of RFI signal which can be downloaded via GCP.
   plotvis.py: Plot visibility to test the simulations.


The simulation data are kept on the Google Cloud Platform at:

    gs://ska1-simulation-data/ska1-mid/rcal_rfi

If you wish to run these or similar simulations, install RASCIL, and use the
shell scripts and python files as template.

Make sure you have the below environment variables set up:

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder

