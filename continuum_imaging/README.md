
The SKA direction-dependent MID simulations are kept on the Google Cloud Platform.

The python command line tool gsutil allows for interacting with the Google Cloud Platform: 

    https://cloud.google.com/storage/docs/gsutil 

After installing gsutil, you may download the simulations as follows: 

    cd continuum_sims_SP-1331
    gsutil rsync gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331 .

If you wish to run these or similar simulations, install RASCIL, and use the
shell scripts and slurm files as template.

Make sure you have the below environment variables set up:

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder

