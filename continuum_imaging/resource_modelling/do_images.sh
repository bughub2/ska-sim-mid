#!/bin/sh
# Create and submit SLURM files to make a sequence of increasingly large images

for int_time in 360
  do
    for npixel in 9216 10240 11264 12288 13312 14336 15360 16384
      do
        sbatch image_B2_5km_csd3_single.slurm ${int_time} ${npixel}
      done
  done


