#!/bin/sh
# Create and submit SLURM files to make a set of Measurement sets. The size is
# determined by the integration time

for int_time in 1440 720 360 180
  do
    sbatch make_B2_5km_csd3.slurm ${int_time}
  done
