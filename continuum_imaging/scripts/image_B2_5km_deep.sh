#!/usr/bin/env bash
#
# Run this from the directory containing the MS
#
python3 ${RASCIL}/rascil/apps/rascil_imager.py  --clean_nmoment 3 --clean_facets 8 --clean_nmajor 10 \
  --clean_threshold 3e-5 \
  --use_dask True --imaging_context ng --imaging_npixel 10240 --imaging_pol stokesI --clean_restored_output integrated \
  --imaging_cellsize 5e-6 --imaging_weighting uniform --imaging_nchan 1 \
  --ingest_vis_nchan 100 --ingest_chan_per_blockvis 16 \
  --ingest_msname ${SSMRESULTS}/5km/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal.ms
