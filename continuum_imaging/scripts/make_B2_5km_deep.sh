#!/usr/bin/env bash
#
# Simulate using all dishes within 5km of the MID core
#

results_dir=${SSMRESULTS}/5km_deep
vp_directory=${SSMRESOURCES}/beam_models/
export PYTHONPATH=${SSMROOT}/src:${PYTHONPATH}

python3 ${SSMROOT}/src/mid_simulation.py --mode nominal --flux_limit 0.00001 \
  --declination -45 --band B2 --pbtype MID_B2 --results ${results_dir}  \
  --integration_time 180 --use_dask True --time_chunk 180 \
  --duration custom --image_pol stokesI --vis_pol linear --sim_pol stokesI \
  --vp_directory ${vp_directory} --nchan 100 --rmax 5e3 \
  --imaging_dft_kernel cpu_numba
