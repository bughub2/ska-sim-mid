#!/usr/bin/env bash
#
# Simulate using all MID
#
results_dir=${SSMRESULTS}/full_array
vp_directory=${SSMRESOURCES}/beam_models/
export PYTHONPATH=${SSMROOT}/src:${PYTHONPATH}

python3 ${SSMROOT}/src/mid_simulation.py --mode nominal --flux_limit 0.0001 \
  --declination -45.0 --band B2 --pbtype MID_B2 --results ${results_dir}  \
  --integration_time 180 --use_dask True --time_chunk 900 \
  --duration custom --image_pol stokesI --vis_pol linear \
  --vp_directory ${vp_directory} --nchan 100
