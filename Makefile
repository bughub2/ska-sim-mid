# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

all: test lint

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
test:
	 python3 setup.py test #| tee ./build/setup_py_test.stdout; #\
	 #mv coverage.xml ./build/reports/code-coverage.xml;

test-report:  ## Run pytest and generate reports required by the badge generation
	pytest -vv -s \
		--capture=no \
		--pydocstyle \
		--durations=5 \
		--cov=src \
		--cov-config=setup.cfg \
		--cov-report=term \
		--cov-report=html \
		--cov-report=xml:code-coverage.xml \
		--no-cov-on-fail \
		--cov-branch \
		--cov-append \
		--junitxml=unit-tests.xml \
		--json-report \
		--json-report-file=htmlcov/report.json \
		tests

lint:  ## Run pylint and display output to stdout + run black and fail if code doesn't comply
	pylint --rcfile=.pylintrc --exit-zero -r n src sizing PSI
	black --check .

lint-report:  ## Run pylint and output an xml report needed for the CI badge.
	pylint --rcfile=.pylintrc --exit-zero -r n -f pylint2junit.JunitReporter src sizing PSI | tee ./linting.xml > /dev/null

docs:  ## build docs
# Outputs docs/build/html
	$(MAKE) -C docs html

clean:  ## Remove all temporary build files
	-rm -rf linting.xml
	-rm -rf htmlcov
	-rm -rf .coverage
	-rm -rf coverage
	-rm -rf coverage.xml
	-rm -rf code-coverage.xml
	-rm -rf unit-tests.xml
	-rm -rf docs/build
	-rm -rf docs/src/_build
	-rm -rf src/*.egg-info
	-rm -rf .eggs


.PHONY: all test lint docs clean
