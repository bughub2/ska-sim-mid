#!/bin/bash
# Get the test data needed from the Google Cloud Platform. Note that these
# file must be enabled explicity for public access
mkdir -p test_resources//beam_models/SKADCBeamPatterns/2019_08_06_SKA_SPFB2/interpolated_frequency/
gsutil cp gs://ska1-simulation-data/resources/mid/beam_models/SKADCBeamPatterns/2019_08_06_SKA_SPFB2/interpolated_frequency/B2_45_1360_real_interpolated.fits test_resources//beam_models/SKADCBeamPatterns/2019_08_06_SKA_SPFB2/interpolated_frequency/
gsutil cp gs://ska1-simulation-data/resources/mid/beam_models/SKADCBeamPatterns/2019_08_06_SKA_SPFB2/interpolated_frequency/B2_45_1360_imag_interpolated.fits test_resources//beam_models/SKADCBeamPatterns/2019_08_06_SKA_SPFB2/interpolated_frequency/
mkdir -p test_resources//beam_models/MEERKATBeamPatterns/interpolated_frequency/
gsutil cp gs://ska1-simulation-data/resources/mid/beam_models/MEERKATBeamPatterns/interpolated_frequency/MeerKAT_VP_60_1360_real_interpolated.fits test_resources//beam_models/MEERKATBeamPatterns/interpolated_frequency/
gsutil cp gs://ska1-simulation-data/resources/mid/beam_models/MEERKATBeamPatterns/interpolated_frequency/MeerKAT_VP_60_1360_imag_interpolated.fits test_resources//beam_models/MEERKATBeamPatterns/interpolated_frequency/
