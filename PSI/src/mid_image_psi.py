"""Simulation of the effect of errors on MID observations

Imaging step
"""
import logging
import pprint
import sys

from rascil.processing_components import (
    create_visibility_from_ms,
    create_image_from_visibility,
    advise_wide_field,
)
from rascil.workflows.rsexecute import (
    invert_list_rsexecute_workflow,
    sum_invert_results_rsexecute,
    weight_list_rsexecute_workflow,
)
from rascil.workflows.rsexecute.execution_support import get_dask_client
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame

pp = pprint.PrettyPrinter()


def initialise_rsexecute(args):
    if args.use_dask == "True":
        client = get_dask_client(
            n_workers=args.nworkers, threads_per_worker=args.nthreads
        )
        rsexecute.set_client(use_dask=True, client=client)
    else:
        rsexecute.set_client(use_dask=False)


def init_logging():
    logging.basicConfig(
        filename="mid_image_psi.log",
        filemode="a",
        format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=logging.INFO,
    )


init_logging()
log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def do_image(args):
    logfile = "{results}/mid_image_psi.log".format(results=args.results)
    print("Writing log file to {}".format(logfile))

    log.info("mid_image:\nStarting PSI imaging\n")

    pp.pprint(vars(args))

    initialise_rsexecute(args)
    rsexecute.init_statistics()

    msname = "{}/{}.ms".format(args.results, args.msname)
    nchan = args.nchan
    spwchan = nchan // args.nspw
    bvis_list = [
        rsexecute.execute(create_visibility_from_ms)(
            msname, start_chan=chan, end_chan=chan + spwchan - 1
        )[0]
        for chan in range(0, nchan, spwchan)
    ]

    make_images(args, bvis_list, msname)

    rsexecute.save_statistics("{}/{}".format(args.results, args.msname))
    rsexecute.close()

    return True


def make_images(args, bvis_list, msname):
    def print_bv(bvis):
        return str(bvis)

    s = rsexecute.execute(print_bv)(bvis_list[0])
    print(rsexecute.compute(s, sync=True))

    cellsize = args.cellsize
    npixel = args.npixel
    if npixel is None:
        advice_low = rsexecute.execute(advise_wide_field)(bvis_list[0])
        advice_low = rsexecute.compute(advice_low, sync=True)
        npixel = advice_low["npixels"]
    if cellsize is None:
        advice_high = rsexecute.execute(advise_wide_field)(bvis_list[-1])
        advice_high = rsexecute.compute(advice_high, sync=True)
        cellsize = advice_high["cellsize"]

    try:
        import xarray

        def flag(bvis):
            bvis["flags"] = xarray.where(
                bvis["uvdist_lambda"] > 0.0, bvis["flags"], 1.0
            )
            return bvis

        bvis_list = [rsexecute.execute(flag)(bv) for bv in bvis_list]
    except:
        print("xarray not installed - cannot flag autocorrelation")

    model_list = [
        rsexecute.execute(create_image_from_visibility)(
            bvis,
            npixel=npixel,
            cellsize=cellsize,
            nchan=1,
            polarisation_frame=PolarisationFrame(args.image_pol),
        )
        for bvis in bvis_list
    ]
    if args.do_weight == "True":
        bvis_list = weight_list_rsexecute_workflow(
            bvis_list, model_list, weighting="robust"
        )
    dirty_list = invert_list_rsexecute_workflow(
        bvis_list,
        template_model_imagelist=model_list,
        context=args.context,
        do_wstacking=False,
        verbosity=2,
        dopsf=args.dopsf == "True",
    )
    dirty_list = sum_invert_results_rsexecute(dirty_list)
    dirty, sumwt = rsexecute.compute(dirty_list, sync=True)
    print(dirty.image_acc.qa_image())
    if args.dopsf == "True":
        dirty.image_acc.export_to_fits(msname.replace(".ms", "_psf.fits"))
    else:
        dirty.image_acc.export_to_fits(msname.replace(".ms", "_dirty.fits"))


def get_args():
    # Get command line inputs
    import argparse

    parser = argparse.ArgumentParser(description="SKA PSI imaging using RASCIL")
    parser.add_argument("--msname", type=str, default="sim_mid_psi", help="MSname")
    parser.add_argument(
        "--ra", type=float, default=0.0, help="Right ascension (degrees)"
    )
    parser.add_argument(
        "--declination", type=float, default=-40.0, help="Declination (degrees)"
    )
    parser.add_argument(
        "--image_pol",
        type=str,
        default="stokesIQUV",
        help="RASCIL polarisation frame for image",
    )
    parser.add_argument(
        "--timeslice", type=int, default=None, help="Number of time slices"
    )
    parser.add_argument(
        "--context", type=str, default="ng", help="Imaging context '2d' | 'ng'"
    )
    parser.add_argument("--npixel", type=int, default=None, help="Number of pixels")
    parser.add_argument(
        "--nchan", type=int, default=10240, help="Number of channels in visibility"
    )
    parser.add_argument(
        "--nspw", type=int, default=512, help="Number of spectral windows"
    )
    parser.add_argument(
        "--cellsize", type=float, default=None, help="Cellsize in radians"
    )
    parser.add_argument("--dopsf", type=str, default=False, help="Make PSF?")
    parser.add_argument("--do_weight", type=str, default=False, help="Weight the data?")
    parser.add_argument(
        "--results", type=str, default="./", help="Directory for results"
    )

    parser.add_argument(
        "--weighting", type=str, default="robust", help="Type of weighting"
    )
    parser.add_argument(
        "--robustness", type=float, default=0.0, help="Robustness for robust weighting"
    )

    parser.add_argument("--nthreads", type=int, default=4, help="Number of threads")
    parser.add_argument(
        "--memory", type=str, default=None, help="Memory per worker (GB)"
    )
    parser.add_argument("--nworkers", type=int, default=4, help="Number of workers")
    parser.add_argument(
        "--use_dask", type=str, default="True", help="Use Dask processing?"
    )

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_args()
    do_image(args)
