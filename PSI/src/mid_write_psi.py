"""SIM-559 PSI MS creation using RASCIL

"""
import copy
import logging
import sys
import time

import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from rascil.processing_components import (
    dft_skycomponent_visibility,
    concatenate_visibility_frequency,
    plot_uvcoverage,
    plot_configuration,
)
from rascil.processing_components.visibility.base import export_visibility_to_ms
from rascil.workflows.rsexecute.execution_support import rsexecute, get_dask_client
from ska_sdp_datamodels.configuration import (
    create_named_configuration,
    select_configuration,
)
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.sky_model import SkyComponent
from ska_sdp_datamodels.visibility import create_visibility


def initialise_rsexecute(args):
    if args.use_dask == "True":
        client = get_dask_client(
            n_workers=args.nworkers, threads_per_worker=args.nthreads
        )
        rsexecute.set_client(use_dask=True, client=client)
    else:
        rsexecute.set_client(use_dask=False)


def init_logging():
    logging.basicConfig(
        filename="mid_image_psi.log",
        filemode="a",
        format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=logging.INFO,
    )


init_logging()
log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def concatenate_visibility_frequency_rsexecute(bvis_list, split=2):
    """Concatenate a list of visibility's, ordered in frequency

    :param bvis_list: List of Blockvis, ordered in frequency
    :param split: Split into
    :return: BlockVis
    """
    if len(bvis_list) > split:
        centre = len(bvis_list) // split
        result = [
            concatenate_visibility_frequency_rsexecute(bvis_list[:centre]),
            concatenate_visibility_frequency_rsexecute(bvis_list[centre:]),
        ]
        return rsexecute.execute(concatenate_visibility_frequency, nout=2)(result)
    else:
        return rsexecute.execute(concatenate_visibility_frequency, nout=2)(bvis_list)


def simulation(args):
    logfile = "{results}/mid_write_psi.log".format(results=args.results)
    print("Writing log file to {}".format(logfile))

    log.info("mid_image:\nStarting PSI imaging\n")

    import pprint

    pp = pprint.PrettyPrinter()
    pp.pprint(vars(args))

    initialise_rsexecute(args)
    rsexecute.init_statistics()

    import dask.distributed

    print(dask.config.config)
    dask.config.set({"distributed.comm.timeouts.connect": 30})

    rsexecute.run(init_logging)

    import pprint

    pp = pprint.PrettyPrinter()
    pp.pprint(vars(args))
    log.info(pp.pformat(vars(args)))

    ra = args.ra
    declination = args.declination
    band = args.band

    if args.duration == "short":
        integration_time = 1.0
        time_range = [-180.0 / 3600.0, 180.0 / 3600.0]
    elif args.duration == "medium":
        integration_time = 10.0
        time_range = [-0.5, 0.5]
    elif args.duration == "long":
        integration_time = 60.0
        time_range = [-4.0, 4.0]
    else:
        args.duration = "custom"
        integration_time = args.integration_time
        time_range = args.time_range

    image_polarisation_frame = PolarisationFrame(args.image_pol)
    vis_polarisation_frame = PolarisationFrame(args.vis_pol)
    log.info("Image polarisation: {}".format(str(image_polarisation_frame)))
    log.info("Vis polarisation: {}".format(str(vis_polarisation_frame)))

    log.info(
        "Simulating {duration} observation: {time_range} hours in integrations of {integration_time}s".format(
            duration=args.duration,
            time_range=time_range,
            integration_time=integration_time,
        )
    )

    rmax = args.rmax

    # Set up details of simulated observation
    nchan = args.nchan
    channel_width = args.channel_width
    if band == "B1":
        if channel_width is None:
            channel_width = 5.0e7 / nchan
        frequency = 0.7650e9 + (numpy.arange(nchan) - nchan // 2) * channel_width
    elif band == "B1LOW":
        if channel_width is None:
            channel_width = 5.0e7 / nchan
        frequency = 0.465e9 + (numpy.arange(nchan) - nchan // 2) * channel_width
    elif band == "B2":
        if channel_width is None:
            channel_width = 0.5 * 1.36e9 / nchan
        frequency = 1.36e9 + (numpy.arange(nchan) - nchan // 2) * channel_width
    elif band == "Ku":
        if channel_width is None:
            channel_width = 5e8 / nchan
        frequency = 12.0e9 + (numpy.arange(nchan) - nchan // 2) * channel_width
    else:
        raise ValueError("Unknown band %s" % band)

    frequency = numpy.array(frequency)
    channel_bandwidth = numpy.repeat(channel_width, len(frequency))

    phasecentre = SkyCoord(
        ra=ra * u.deg, dec=declination * u.deg, frame="icrs", equinox="J2000"
    )

    # comps = create_test_skycomponents_from_s3(frequency=frequency, phasecentre=phasecentre,
    #                                      polarisation_frame=vis_polarisation_frame, flux_limit=0.3)

    flux = numpy.zeros([len(frequency), image_polarisation_frame.npol])
    flux[:, 0] = 1.0

    mid = create_named_configuration(args.configuration, rmax=args.rmax)
    if args.antennas is None:
        names = ["SKA057", "SKA062", "SKA072", "SKA071", "SKA002", "SKA049"]
        mid = select_configuration(mid, names)
        log.info("Standard antennas are {}".format(mid.names))
    else:
        mid = select_configuration(mid, args.antennas)
        log.info("Selected antennas are {}".format(mid.names))

    times = numpy.arange(time_range[0] * 3600, time_range[1] * 3600, integration_time)
    times *= numpy.pi / 43200.0

    nspw = args.nspw
    chunk_frequencys = numpy.array_split(frequency, nspw)
    chunk_channel_bandwidths = numpy.array_split(channel_bandwidth, nspw)
    scale = 0.2
    source1 = SkyCoord(
        ra=(ra + 2.0 * scale) * u.deg,
        dec=(declination + 1.5 * scale) * u.deg,
        frame="icrs",
        equinox="J2000",
    )
    source2 = SkyCoord(
        ra=(ra - 1.0 * scale) * u.deg,
        dec=(declination - 1.0 * scale) * u.deg,
        frame="icrs",
        equinox="J2000",
    )
    chunk_flux1 = numpy.array_split(flux, nspw)
    chunk_flux2 = numpy.array_split(flux / 2.0, nspw)

    chunk_comps = [
        [
            SkyComponent(
                frequency=chunk_frequencys[spw],
                direction=source1,
                polarisation_frame=image_polarisation_frame,
                flux=chunk_flux1[spw],
            ),
            SkyComponent(
                frequency=chunk_frequencys[spw],
                direction=source2,
                polarisation_frame=image_polarisation_frame,
                flux=chunk_flux2[spw],
            ),
        ]
        for spw in range(nspw)
    ]

    assert len(chunk_channel_bandwidths) == len(chunk_frequencys)

    def create_fill(spw):
        bvis = create_visibility(
            mid,
            times,
            frequency=chunk_frequencys[spw],
            channel_bandwidth=chunk_channel_bandwidths[spw],
            weight=1.0,
            phasecentre=phasecentre,
            polarisation_frame=vis_polarisation_frame,
        )
        if args.badantenna > 0:
            badmid = copy.deepcopy(mid)
            badmid["xyz"][args.badantenna, 0] += 1.0
            badbvis = create_visibility(
                badmid,
                times,
                frequency=chunk_frequencys[spw],
                channel_bandwidth=chunk_channel_bandwidths[spw],
                weight=1.0,
                phasecentre=phasecentre,
                polarisation_frame=vis_polarisation_frame,
            )

            badbvis = dft_skycomponent_visibility(badbvis, chunk_comps[spw])
            bvis["vis"] = badbvis["vis"]
            print(numpy.max(numpy.abs(badbvis["uvw"] - bvis["uvw"])))
        else:
            bvis = dft_skycomponent_visibility(bvis, chunk_comps[spw])

        return bvis

    bvis_list = [rsexecute.execute(create_fill, nout=1)(spw) for spw in range(nspw)]
    entire_bvis = concatenate_visibility_frequency_rsexecute(
        bvis_list, split=args.split
    )
    log.info("Start to construct, fill, and combine MSes")
    start_time = time.time()
    entire_bvis = rsexecute.compute(entire_bvis, sync=True)
    ms_createtime = time.time() - start_time
    log.info(
        "Time to construct, fill and combine MSes = {:.3f} seconds".format(
            ms_createtime
        )
    )
    del bvis_list
    log.info("Final visibility: {}".format(entire_bvis))

    rsexecute.save_statistics("{results}/mid_write_psi".format(results=args.results))
    rsexecute.close()

    print(
        "Size of final visibility = {:.3f} GB".format(entire_bvis.visibility_acc.size())
    )

    msfile = "{}/{}.ms".format(args.results, args.msname)
    log.info("Writing visibility to MS")
    start_time = time.time()
    export_visibility_to_ms(msfile, [entire_bvis])
    ms_writetime = time.time() - start_time
    log.info("Time to write visibility as MS = {:.3f} seconds".format(ms_writetime))

    print("visibility size:\n{}".format(entire_bvis.visibility_acc.datasizes()))

    plotfile = "{}/{}_config.png".format(args.results, args.msname)
    plot_configuration(entire_bvis.configuration, plotfile=plotfile, label=False)

    plotfile = "{}/{}_uvcoverage.png".format(args.results, args.msname)
    plot_uvcoverage([entire_bvis], plotfile=plotfile)

    return True


def cli_parser():
    global parser
    import argparse

    parser = argparse.ArgumentParser(
        description="Simulate time and frequency dense PSI observations"
    )
    parser.add_argument(
        "--rmax",
        type=float,
        default=200.0,
        help="Maximum distance of dish from centre (m)",
    )
    parser.add_argument(
        "--configuration",
        type=str,
        default="MID",
        help="MID Configuration: MID | MEERKAT+",
    )
    parser.add_argument(
        "--antennas",
        type=str,
        default=None,
        nargs="*",
        help="antenna names to include (default is all)",
    )
    parser.add_argument("--badantenna", type=int, default=-1, help="Bad antenna")
    parser.add_argument(
        "--howbad", type=str, default=1, help="How many meters to move bad antenna"
    )
    parser.add_argument(
        "--ra",
        type=float,
        default=0.0,
        help="Right ascension of phase centre (degrees)",
    )
    parser.add_argument(
        "--declination",
        type=float,
        default=-55.0,
        help="Declination of phase centre (degrees)",
    )
    parser.add_argument(
        "--band", type=str, default="B2", help="Band B1LOW | B1 | B2 | Ku"
    )
    parser.add_argument(
        "--nchan", type=int, default=1024, help="Number of frequency channels"
    )
    parser.add_argument(
        "--nspw", type=int, default=8, help="Number of spectral windows"
    )
    parser.add_argument(
        "--channel_width",
        type=float,
        default=None,
        help="Channel bandwidth (Hz) (default is to calculate" " from frequency",
    )
    parser.add_argument(
        "--integration_time", type=float, default=0.14, help="Integration time (s)"
    )
    parser.add_argument(
        "--time_range",
        type=float,
        nargs=2,
        default=[0, 0.16667],
        help="Time range in hour angle",
    )
    parser.add_argument(
        "--image_pol",
        type=str,
        default="stokesIQUV",
        help="RASCIL polarisation frame for image:" " stokesI | stokes IQ | stokesIQUV",
    )
    parser.add_argument(
        "--vis_pol",
        type=str,
        default="linear",
        help="RASCIL polarisation frame for visibility: " "linear | linearnp",
    )
    parser.add_argument(
        "--duration",
        type=str,
        default="long",
        help="Type of duration: long or medium or short",
    )
    parser.add_argument(
        "--results", type=str, default="./", help="Directory for results"
    )
    parser.add_argument(
        "--msname",
        type=str,
        default="sim_mid_psi",
        help="Root name of MS (i.e. without .ms)",
    )

    parser.add_argument(
        "--split", type=int, default=2, help="In concat, type of tree e.g. 2->binary"
    )

    parser.add_argument(
        "--nthreads", type=int, default=1, help="Number of threads per worker"
    )
    parser.add_argument(
        "--processes", type=int, default=1, help="Number of processes per worker"
    )
    parser.add_argument(
        "--memory", type=str, default=None, help="Memory per worker (GB)"
    )
    parser.add_argument("--nworkers", type=int, default=4, help="Number of workers")
    parser.add_argument(
        "--use_dask", type=str, default="True", help="Use dask processing?"
    )

    parser.add_argument(
        "--interface", type=str, default="ib0", help="Network interface"
    )

    return parser


if __name__ == "__main__":
    parser = cli_parser()

    args = parser.parse_args()

    simulation(args)
