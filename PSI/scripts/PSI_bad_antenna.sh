# SIM-559 tests
if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT/src:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${SSMROOT}/results
fi
echo "SSMRESULTS : $SSMRESULTS"


python3 ${SSMROOT}/PSI/src/mid_write_psi.py \
--nchan 10240 --nchan 128 --time_range 0 0.16667 --duration custom --integration_time 0.28 \
--vis_pol circular --image_pol stokesIQUV --results ${SSMRESULTS} \
--antennas SKA057 SKA062 SKA072 SKA071 SKA002 SKA049 --msname sim_mid_psi_good

python3 ${SSMROOT}/PSI/src/mid_write_psi.py \
--nchan 10240 --nchan 128 --time_range 0 0.16667 --duration custom --integration_time 0.28 \
--vis_pol circular --image_pol stokesIQUV --badantenna 3 --results ${SSMRESULTS} \
--antennas SKA057 SKA062 SKA072 SKA071 SKA002 SKA049 --msname sim_mid_psi_bad

python3 ${SSMROOT}/PSI/src/mid_image_psi.py --nspw 16 --nchan 128 --npixel 256  --results ${SSMRESULTS} \
--dopsf False --context ng --use_dask True --image_pol stokesIQUV --msname sim_mid_psi_good

python3 ${SSMROOT}/PSI/src/mid_image_psi.py --nspw 16 --nchan 128 --npixel 256  --results ${SSMRESULTS} \
--dopsf False --context ng --use_dask True --image_pol stokesIQUV --msname sim_mid_psi_bad
