# SIM-559 tests

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT/src:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${SSMROOT}/results
fi
echo "SSMRESULTS : $SSMRESULTS"

python3 ${SSMROOT}/PSI/src/mid_image_psi.py --nworkers 1 --nthreads 64  \
--memory 1024GB --use_dask True --results ${SSMRESULTS} --nspw 8 \
--npixel 256 --context ng
