# SIM-559 tests

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${SSMROOT}/results
fi
echo "SSMRESULTS : $SSMRESULTS"

python3 ${SSMROOT}/PSI/src/mid_write_psi.py --duration custom --integration_time 0.14 --time_range 0 0.16666 \
--nchan 10240 --nworkers 1 --processes 1 --nthreads 64   --memory 1024GB  \
--use_dask True --nspw 128 --interface ib0 --results ${SSMRESULTS} \
--antennas SKA057 SKA062 SKA072 SKA071 SKA002 SKA049

