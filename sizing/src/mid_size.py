"""Simulation of the effect of errors on MID observations: sizing


"""
import logging
import pprint
import sys

import matplotlib.pyplot as plt
import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility
from ska_sdp_datamodels.configuration import create_named_configuration
from rascil.processing_components import (
    advise_wide_field,
    export_visibility_to_ms,
    plot_uvcoverage,
)

pp = pprint.PrettyPrinter()

from src.common import get_directory_size


def init_logging():
    logging.basicConfig(
        filename="mid_size.log",
        filemode="a",
        format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=logging.INFO,
    )


init_logging()
log = logging.getLogger("rascil-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def simulation(args):

    band = "B2"
    vis_polarisation_frame = PolarisationFrame("linear")

    import pprint

    pp = pprint.PrettyPrinter()

    log.info("mid_size: Starting MID simulation sizing\n")
    pp.pprint(vars(args))

    rmax = args.rmax

    # Set up details of simulated observation
    if band == "B1":
        frequency = [0.765e9]
    elif band == "B2":
        frequency = [1.36e9]
    elif band == "Ku":
        frequency = [12.179e9]
    else:
        raise ValueError("Unknown band %s" % band)

    frequency = numpy.array(frequency)
    channel_bandwidth = numpy.array([args.fractional_bandwidth * frequency[0]])

    phasecentre = SkyCoord(
        ra=args.ra * u.deg, dec=args.declination * u.deg, frame="icrs", equinox="J2000"
    )

    config = create_named_configuration("MIDR5", rmax=args.rmax)
    time_rad = numpy.array(args.time_range) * numpy.pi / 12.0
    times = numpy.arange(
        time_rad[0], time_rad[1], args.integration_time * numpy.pi / 43200.0
    )
    bvis_list = [
        create_visibility(
            config,
            times=times,
            phasecentre=phasecentre,
            polarisation_frame=vis_polarisation_frame,
            frequency=[freq],
            channel_bandwidth=channel_bandwidth,
            integration_time=args.integration_time,
        )
        for freq in frequency
    ]

    if args.verbose == "True":
        plt.clf()
        plot_uvcoverage(bvis_list)
        plt.show(block=False)

    advice = advise_wide_field(
        bvis_list[0],
        guard_band_image=args.guardband,
        delA=args.dela,
        verbose=args.verbose == "True",
    )

    nfreq = int(channel_bandwidth / advice["freq_sampling_primary_beam"])
    channel_bandwidth = channel_bandwidth / nfreq
    times = numpy.arange(
        time_rad[0],
        time_rad[1],
        advice["time_sampling_primary_beam"] * numpy.pi / 43200.0,
    )
    bvis_list = [
        create_visibility(
            config,
            times=times,
            phasecentre=phasecentre,
            polarisation_frame=vis_polarisation_frame,
            frequency=[freq],
            channel_bandwidth=channel_bandwidth,
            integration_time=advice["time_sampling_primary_beam"],
        )
        for freq in frequency
    ]
    if args.verbose == "True":
        log.info("mid_size: {} ".format(bvis_list[0]))

    export_visibility_to_ms(args.output_msname, bvis_list)

    size = nfreq * get_directory_size(args.output_msname) / 1024 / 1024 / 1024
    msg = (
        "mid_size: Using only dishes within {rmax:.1f}m of the array centre requires {nfreq} channels "
        "of {channel_bandwidth:.3g}Hz and integration time"
        " {integration_time:.1f}s, the MS size is {size:.3f}GB".format(
            rmax=rmax,
            size=size,
            nfreq=nfreq,
            channel_bandwidth=channel_bandwidth[0],
            integration_time=advice["time_sampling_primary_beam"],
        )
    )
    log.info(msg)
    msg = (
        "mid_size: The time and frequency sampling is to provide no more than 2% smearing "
        "at the half power point of the primary beam."
    )
    log.info(msg)
    npixels = advice["npixels23"]
    image_size = npixels * npixels * 8 * 4 / 1024 / 1024 / 1024
    log.info(
        "mid_size: Image has shape {npixels} by {npixels}, 4 polarisations, and size {image_size:.3f}GB".format(
            npixels=npixels, image_size=image_size
        )
    )
    log.info(
        "mid_size: W processing requires {w_planes} w planes of step {wstep:.1f} wavelengths "
        "and maximum support {support} pixels".format(
            w_planes=advice["wprojection_planes"],
            wstep=advice["w_sampling_primary_beam"],
            support=advice["nwpixels"],
        )
    )
    msg = "mid_size: Allowing fractional bandwidth {fractional_bandwidth:.3f}  to fill in the uv sampling. ".format(
        fractional_bandwidth=args.fractional_bandwidth
    )
    log.info(msg)

    return True


def cli_parser():
    global parser
    import argparse

    parser = argparse.ArgumentParser(
        description="Calculate MeasurementSets sizes for MID observations"
    )
    parser.add_argument(
        "--output_msname",
        type=str,
        default="mid_simulation.ms",
        help="MeasurementSet to be written",
    )

    parser.add_argument(
        "--context", type=str, default="s3sky", help="s3sky or singlesource or null"
    )

    # Observation definition
    parser.add_argument(
        "--ra", type=float, default=+15.0, help="Right ascension (degrees)"
    )
    parser.add_argument(
        "--declination", type=float, default=-45.0, help="Declination (degrees)"
    )
    parser.add_argument(
        "--rmax",
        type=float,
        default=1e3,
        help="Maximum distance of station from centre (m)",
    )
    parser.add_argument("--band", type=str, default="B2", help="Band")
    parser.add_argument(
        "--integration_time", type=float, default=80.0, help="Integration time (s)"
    )
    parser.add_argument(
        "--time_range",
        type=float,
        nargs=2,
        default=[-4.0, 4.0],
        help="Time range in hours",
    )
    parser.add_argument(
        "--fractional_bandwidth", type=float, default=0.1, help="Fractional bandwidth"
    )
    parser.add_argument(
        "--guardband",
        type=float,
        default=2.0,
        help="Size of field of view relative to primary beam",
    )
    parser.add_argument(
        "--dela",
        type=float,
        default=0.02,
        help="Allowable degradation of point source at half power point of primary beam",
    )
    parser.add_argument("--verbose", type=str, default="False", help="Verbose output?")

    return parser


if __name__ == "__main__":

    parser = cli_parser()
    args = parser.parse_args()
    simulation(args)
