
Typical output from make_sizes.sh:

create_visibility: flagged 0/360 times below elevation limit 0.261799 (rad)
create_visibility: flagged 0/42 times below elevation limit 0.261799 (rad)

    mid_size: Using only dishes within 1000.0m of the array centre requires 12 channels of 1.13e+07Hz and integration time 685.8s, the MS size is 0.331GB
    mid_size: The time and frequency sampling is to provide no more than 2% smearing at the half power point of the primary beam.
    mid_size: Image has shape 768 by 768, 4 polarisations, and size 0.018GB
    mid_size: W processing requires 58 w planes of step 238.8 wavelengths and maximum support 0 pixels
    mid_size: Allowing fractional bandwidth 0.100  to fill in the uv sampling. 
    create_visibility: flagged 0/360 times below elevation limit 0.261799 (rad)
    create_visibility: flagged 0/362 times below elevation limit 0.261799 (rad)
    mid_size: Using only dishes within 10000.0m of the array centre requires 108 channels of 1.26e+06Hz and integration time 79.7s, the MS size is 51.265GB
    mid_size: The time and frequency sampling is to provide no more than 2% smearing at the half power point of the primary beam.
    mid_size: Image has shape 8192 by 8192, 4 polarisations, and size 2.000GB
    mid_size: W processing requires 477 w planes of step 238.8 wavelengths and maximum support 14 pixels
    mid_size: Allowing fractional bandwidth 0.100  to fill in the uv sampling. 
    create_visibility: flagged 0/360 times below elevation limit 0.261799 (rad)
    create_visibility: flagged 0/3861 times below elevation limit 0.261799 (rad)
    mid_size: Using only dishes within 100000.0m of the array centre requires 1158 channels of 1.17e+05Hz and integration time 7.5s, the MS size is 8462.467GB
    mid_size: The time and frequency sampling is to provide no more than 2% smearing at the half power point of the primary beam.
    mid_size: Image has shape 98304 by 98304, 4 polarisations, and size 288.000GB
    mid_size: W processing requires 4948 w planes of step 238.8 wavelengths and maximum support 160 pixels
    mid_size: Allowing fractional bandwidth 0.100  to fill in the uv sampling. 
