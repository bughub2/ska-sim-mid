#!/bin/bash -e

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT:$PYTHONPATH

python3 ${SSMROOT}/sizing/src/mid_size.py --rmax 1e3
python3 ${SSMROOT}/sizing/src/mid_size.py --rmax 1e4
python3 ${SSMROOT}/sizing/src/mid_size.py --rmax 2e5

