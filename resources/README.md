
This directory is available on the GCP under project ska1-simulations-270612 at:

    gs://ska1-simulation-data/ska1-mid/screens

The package https://github.com/shrieks/ARatmospy must be in the path. The current version won't work 
for python3 so you should use the forked version at:

    git clone https://github.com/timcornwell/ARatmospy

e.g.

    PYTHONPATH=$PYTHONPATH:~/Code/ARatmospy
    
The required files can be created using:

    make all
    
Note that the files are quite large:

     11G    iono_screen_medium.fits
    7.2G    iono_screen_long.fits
    11G     tropo_screen_medium.fits
    7.2G    tropo_screen_long.fits

  
      
