""" This checks the frequency and elevation interpolations. It's not meant for general use.

"""
import logging
import sys

log = logging.getLogger()
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))

from src.common import get_vp_frequency, get_vp_elevation


def main():
    print("Checking frequency interpolation")

    dir = "../resources/beam_models"
    for telescope in ["MEERKAT_B2", "MID_B2"]:
        try:
            print(telescope)
            vp = get_vp_frequency(telescope, dir, fixpol=False)
        except ValueError as err:
            print(err)

    print("Checking elevation interpolation")

    for telescope in ["MID_B1", "MID_B2", "MID_Ku"]:
        try:
            print(telescope)
            vp = get_vp_elevation(telescope, dir, el=30.0, fixpol=False)
        except ValueError as err:
            print(err)


if __name__ == "__main__":
    main()
