#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
from setuptools import setup

# Bail on Python < 3
assert sys.version_info[0] >= 3

with open("README.md") as readme_file:
    readme = readme_file.read()

# List of requirements cannot contain lines that start with #
# can neither work with git+https ones, hence we'll remove it
# and add it back below with the correct syntax for setup.py
reqs = [
    line.strip()
    for line in open("requirements.txt").readlines()
    if not line.strip().startswith("#")
    and line.strip() != ""
    and not line.startswith("git")
    and not line.strip().startswith("--extra-index-url")
    and not line.strip().startswith("--index-url")
]

setup(
    name="ska_mid-simulations",
    version="0.1.0",
    description="",
    long_description=readme + "\n\n",
    author="Tim Cornwell",
    author_email="realtimcornwell@gmail.com",
    url="https://github.com/ska-telescope/sim/ska-sim-mid",
    packages=setuptools.find_namespace_packages(where="src", include=["ska.*"]),
    package_dir={"": "src"},
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    classifiers=[
        "Development Status :: 2 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    test_suite="tests",
    install_requires=[],  # FIXME: add your package's dependencies to this list
    setup_requires=[
        # dependency for `python setup.py test`
        "pytest-runner",
        # dependencies for `python setup.py build_sphinx`
        "sphinx",
        "recommonmark",
    ],
    tests_require=[
        "pytest",
        "pytest-cov",
        "pytest-json-report",
        "pycodestyle",
    ],
    extras_require={
        "dev": ["prospector[with_pyroma]", "yapf", "isort"],
    },
)
