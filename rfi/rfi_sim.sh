#!/usr/bin/env bash

# necessary for imports to work
# we are adding the git directory ska-sim-mid to PYTHONPATH
git_root_dir=`git rev-parse --show-toplevel`
export PYTHONPATH="${git_root_dir}:$PYTHONPATH"

input_data="data/aeronautical_v2_SKA_Mid_coord/aeronautical_sim_datacube_20190313_all.h5"
ms_out="simulate_rfi.ms"
n_channels = 8

echo "----------------------------"
echo " Running RFI simulations... "
echo "----------------------------"

python mid_rfi_simulation.py --input_file ${input_data} --msout ${ms_out} --nchannels ${n_channels}


echo "--------------------------"
echo " Running RASCIL imager... "
echo "--------------------------"

python $RASCIL/rascil/apps/rascil_imager.py --ingest_msname ${ms_out} --ingest_dd 0 \
--ingest_vis_nchan ${n_channels} --ingest_chan_per_blockvis 1 --mode invert --imaging_cellsize 0.00003 \
--imaging_npixel 768 || exit

echo "--------------------------"
echo " Running RASCIL visualise "
echo "--------------------------"

python $RASCIL/rascil/apps/rascil_vis_ms.py --ingest_msname ${ms_out} || exit

echo "--------------------------"
echo " Finished.                "
echo "--------------------------"